package com.novel.system.domain;

import com.novel.framework.base.BaseModel;
import com.novel.framework.validate.groups.AddGroup;
import com.novel.framework.validate.groups.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

/**
 * 菜单权限表 sys_menu
 *
 * @author novel
 * @date 2019/4/16
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class SysMenu extends BaseModel {
    private static final long serialVersionUID = 1L;

    /**
     * 菜单名称
     */
    @NotBlank(message = "菜单名称不能为空", groups = {AddGroup.class})
    @Size(max = 10, message = "菜单名称长度不能超过10个字符", groups = {AddGroup.class, EditGroup.class})
    private String menuName;

    /**
     * 父菜单名称
     */
    private String parentName;

    /**
     * 父菜单ID
     */
    @Range(min = 0, message = "父菜单选择不正确", groups = {AddGroup.class, EditGroup.class})
    private Long parentId;

    /**
     * 显示顺序
     */
    @NotBlank(message = "菜单排序不能为空", groups = {AddGroup.class})
    @Range(min = 0, message = "菜单排序不正确", groups = {AddGroup.class, EditGroup.class})
    private String orderNum;

    /**
     * 菜单URL
     */
    @Size(max = 30, message = "菜单地址长度不能超过30个字符", groups = {AddGroup.class, EditGroup.class})
    private String url;

    /**
     * 类型:M目录,C菜单,F按钮
     */
    @NotBlank(message = "菜单类型不能为空", groups = {AddGroup.class})
    @Pattern(regexp = "^M|C|F$", message = "菜单类型错误", groups = {AddGroup.class, EditGroup.class})
    private String menuType;

    /**
     * 菜单状态:0显示,1隐藏
     */
//    @NotBlank(message = "菜单状态不能为空", groups = {AddGroup.class})
    @Pattern(regexp = "^|0|1$", message = "菜单状态错误", groups = {AddGroup.class, EditGroup.class})
    private String visible;

    /**
     * 权限字符串
     */
    @Size(max = 30, message = "权限字符串长度不能超过30个字符", groups = {AddGroup.class, EditGroup.class})
    private String perms;

    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 组件地址
     */
    @Size(max = 30, message = "组件地址长度不能超过30个字符", groups = {AddGroup.class, EditGroup.class})
    private String component;
    /**
     * 菜单默认路由
     */
    private String redirect;

    /**
     * 子菜单
     */
    private List<SysMenu> children = new ArrayList<>();

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(String orderNum) {
        this.orderNum = orderNum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getPerms() {
        return perms;
    }

    public void setPerms(String perms) {
        this.perms = perms;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<SysMenu> getChildren() {
        return children;
    }

    public void setChildren(List<SysMenu> children) {
        this.children = children;
    }
}
