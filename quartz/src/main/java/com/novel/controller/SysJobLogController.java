package com.novel.controller;

import com.novel.domain.SysJobLog;
import com.novel.framework.annotation.Log;
import com.novel.framework.base.BaseController;
import com.novel.framework.enums.BusinessType;
import com.novel.framework.result.Result;
import com.novel.framework.utils.excel.ExcelUtils;
import com.novel.framework.web.page.TableDataInfo;
import com.novel.service.SysJobLogService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 调度日志操作处理
 *
 * @author novel
 * @date 2020/3/2
 */
@RestController
@RequestMapping("/monitor/jobLog")
public class SysJobLogController extends BaseController {

    private final SysJobLogService jobLogService;

    public SysJobLogController(SysJobLogService jobLogService) {
        this.jobLogService = jobLogService;
    }

    /**
     * 任务日志列表
     *
     * @param jobLog 任务日志查询条件
     * @return 任务日志
     */
    @RequiresPermissions("monitor:job:list")
    @GetMapping("/list")
    public TableDataInfo list(SysJobLog jobLog) {
        startPage();
        List<SysJobLog> list = jobLogService.selectJobLogList(jobLog);
        return getDataTable(list);
    }

    /**
     * 删除任务日志
     *
     * @param ids 任务日志id数组
     * @return 删除结果
     */
    @Log(title = "调度日志", businessType = BusinessType.DELETE)
    @RequiresPermissions("monitor:job:remove")
    @DeleteMapping("/remove")
    public Result remove(Long[] ids) {
        return toAjax(jobLogService.deleteJobLogByIds(ids));
    }

    /**
     * 清空任务日志
     *
     * @return 清空结果
     */
    @Log(title = "调度日志", businessType = BusinessType.CLEAN)
    @RequiresPermissions("monitor:job:remove")
    @DeleteMapping("/clean")
    public Result clean() {
        return toAjax(jobLogService.cleanJobLog());
    }

    /**
     * 导出任务日志
     *
     * @param jobLog 导出查询条件
     * @return 任务日志导出结果
     */
    @Log(title = "调度日志", businessType = BusinessType.EXPORT)
    @RequiresPermissions("monitor:job:export")
    @GetMapping("/export")
    public Result export(SysJobLog jobLog) {
        startPage();
        List<SysJobLog> list = jobLogService.selectJobLogList(jobLog);
        String fileName = ExcelUtils.exportExcelToFile(list, "调度日志", SysJobLog.class);
        Map<String, Object> map = new HashMap<>(1);
        map.put("fileName", fileName);
        return toAjax(map);
    }
}
