package com.novel.constant;

import com.novel.common.constants.Constants;

/**
 * @author 李振
 * @date 2020/3/25
 */
public interface CommonConstant extends Constants {

    /**
     * 自动去除表前缀
     */
    public static String AUTO_REOMVE_PRE = "true";
}
